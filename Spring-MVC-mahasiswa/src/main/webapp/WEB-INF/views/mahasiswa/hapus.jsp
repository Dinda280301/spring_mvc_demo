<!DOCTYPE web-app PUBLIC "-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN" "http://java.sun.com/dtd/web-app_2_3.dtd" >
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%
	request.setAttribute("contextName",request.getContextPath());
%>
<html>
<head>
<style>
.redtext {
	color: red;
}
</style>
</head>
<body>
	<p>Welcome to Spring MVC Tutorial</p>
	
	<form action="${contextName}/mhs" method="post">
	<input type="hidden" name="mode" value="hapus">
	<input type="hidden" name="id" value="${mahasiswa.id}">
	NIM <input type="text"  readonly name="nim" value="${mahasiswa.nim}"><br>
	Nama <input type="text" readonly name="nama" value="${mahasiswa.nama}"><br>
	Alamat <input type="text" readonly name="alamat" value="${mahasiswa.alamat}"><br>
	TTL <input type="text" readonly name="dob" value="${mahasiswa.dob}"><br>
	<p class="redtext">*Data tidak dapat diubah</p>
	<button type="submit">Hapus</button>
	</form>
	
	<ol>
	<c:forEach var="mhs" items="${mhslist}">
	<li>${mhs.nim} - ${mhs.nama} - <a href="${contextName}/mhs/edit?id=${mhs.id}">Edit</a> <a href="${contextName}/mhs/hapus?id=${mhs.id}">Hapus</a></li>
	</c:forEach>
	</ol>
</body>
</html>
